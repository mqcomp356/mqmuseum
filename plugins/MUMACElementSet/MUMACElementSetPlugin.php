<?php

class MUMACElementSetPlugin extends Omeka_Plugin_AbstractPlugin
{
	/* Element Set Name*/
    private $_elementSetName = 'MUMAC';

    protected $_hooks = array(
        'install',
        'uninstall',
        'admin_append_to_plugin_uninstall_message',
        'public_theme_header',
    );

    protected $_filters = array(
        'response_contexts',
        'action_contexts',
    );

    /* Plugin installation function */
    public function hookInstall()
    {
        $this->_installOptions();

        /* Load Element Field Names */
        require_once('elements.php');

        /* Check for current installation of plugin on server*/
        if ($this->_getElementSet($this->_elementSetName)) {
            throw new Omeka_Plugin_Installer_Exception('This ElementSet("' . $this->_elementSetName . '" ) is already installed
			on this server. Please uninstall before installing again. ty :)');
        }

		/* Insert Element fields*/
        insert_element_set($elementSetMetadata, $elements);
    }

    /* Plugin uninstall function*/
    public function hookUninstall()
    {
        $this->_deleteElementSet($this->_elementSetName);

        $this->_uninstallOptions();
    }

    /* Uninstallation warning*/
    public function hookAdminAppendToPluginUninstallMessage()
    {
        echo '<p><strong>' . __('Warning') . '</strong>:'
            . __('Removing ("' . $this->_elementSetName . '") may cause issues with current entries using this element set, 
			proceed with caution')
            . '</p>';
    }

    public function hookPublicThemeHeader()
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        if ($request->getControllerName() == 'items' && $request->getActionName() == 'show') {
            echo '<link rel="alternate" type="application/rss+xml" href="' . record_url(get_current_record('item')) . '?output=MUMAC" id="MUMAC"/>' . PHP_EOL;
        }
        if ($request->getControllerName() == 'items' && $request->getActionName() == 'browse') {
            echo '<link rel="alternate" type="application/rss+xml" href="' . record_url(get_current_record('item')) . '?output=MUMAC" id="MUMAC"/>' . PHP_EOL;
        }
    }

    public function filterResponseContexts($contexts)
    {
        $contexts['MUMAC'] = array(
            'suffix'  => 'MUMAC',
            'headers' => array('Content-Type' => 'text/xml'),
        );

        return $contexts;
    }

    public function filterActionContexts($contexts, $controller)
    {
        if ($controller['controller'] instanceof ItemsController) {
            $contexts['show'][] = 'MUMAC';
            $contexts['browse'][] = 'MUMAC';
        }

        return $contexts;
    }

    private function _getElementSet($elementSetName)
    {
        return $this->_db
            ->getTable('ElementSet')
            ->findByName($elementSetName);
    }

    private function _deleteElementSet($elementSetName)
    {
        $elementSet = $this->_getElementSet($elementSetName);

        if ($elementSet) {
            $elements = $elementSet->getElements();
            foreach ($elements as $element) {
                $element->delete();
            }
            $elementSet->delete();
        }
    }
}
