<!DOCTYPE html>
<html class="<?php echo get_theme_option('Style Sheet'); ?>" lang="<?php echo get_html_lang(); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0, minimum-scale=1.0, user-scalable=yes" />
    <?php if ($description = option('description')): ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php endif; ?>
	<!---Start Emel Editing -->
    <?php
    if (isset($title)) {
        $titleParts[] = strip_formatting($title);
    }
    $titleParts[] = option('site_title');
    ?>
    <title><?php echo implode(' &middot; ', $titleParts); ?></title>

    <?php echo auto_discovery_link_tags(); ?>

    <?php fire_plugin_hook('public_head',array('view'=>$this)); ?>
    <!-- Stylesheets -->
    <?php
    queue_css_file(array('iconfonts','style'));
	
    echo head_css();
    ?>
    <!-- JavaScripts -->
    <?php queue_js_file('vendor/selectivizr', 'javascripts', array('conditional' => '(gte IE 6)&(lte IE 8)')); ?>
    <?php queue_js_file('vendor/respond'); ?>
    <?php queue_js_file('vendor/jquery-accessibleMegaMenu'); ?>

    <?php queue_js_file('berlin'); ?>
    <?php queue_js_file('globals'); ?>
    <?php echo head_js(); ?>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
	<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
	<script>

		$(document).ready(function() {
			var divClone3D = $("#3dBox").clone();
			var divCloneLink = $("#itemRight a").clone();
			$( "#tabs" ).tabs({

			  collapsible: true

			});
			$("#thumbnails .item-file").click(function(e){
				e.preventDefault();
				var key = $(this).html();
				$('#3dBox').replaceWith('<div id="3dBox" style="height:370px">'+key+'</div>');
				$('#itemRight a').replaceWith('<a href="#">Click here for 3D Viewer</a>')
				});
			$('#itemRight').on('click','a',function(){
					$("#3dBox").replaceWith(divClone3D)
					$('#itemRight a').replaceWith(divCloneLink);
			});
			

		});
	
	 </script>
	
</head>
 <?php echo body_tag(array('id' => @$bodyid, 'class' => @$bodyclass)); ?>
    <a href="#content" id="skipnav"><?php echo __('Skip to main content'); ?></a>
    <?php fire_plugin_hook('public_body', array('view'=>$this)); ?>
        <header role="banner">
            <?php fire_plugin_hook('public_header', array('view'=>$this)); ?>
            <div id="site-title"><?php echo link_to_home_page(theme_logo()); ?></div>
            <nav>
         <div id="primary-nav" role="navigation">
             <?php
                  echo public_nav_main();
             ?>
         </div>
          <div id="search-container" role="search" value="find us">
                <?php if (get_theme_option('use_advanced_search') === null || get_theme_option('use_advanced_search')): ?>
             <?php echo search_form(array('show_advanced' => true)); ?>
                <?php else: ?>
                <?php echo search_form(); ?>
                <?php endif; ?>
</div>
     
    </nav>
         <div id="mobile-nav" role="navigation" aria-label="<?php echo __('Mobile Navigation'); ?>">
             <?php
                  echo public_nav_main();
             ?>
         </div>



            <!-- C356 add social --->
            <div class="utilities">
           <!-- <p id="facebook"</p><a href="http://www.facebook.com/pages/Macquarie-University-Museum-of-Ancient-Cultures/129606210445366">Facebook - MAC</a> -->  
	    <!--ul class="socialMedia">-->
		<!--li class="facebook"><a href="http://www.facebook.com/pages/Macquarie-University-Museum-of-Ancient-Cultures/129606210445366">Facebook</a><-->
		<!--li class="twitter"><a href="https://twitter.com/austmus">Twitter</a><-->
		<!--li class="instagram"><a href="http://instagram.com/australianmuseum">Instagram</a><-->
		<!--li class="youtube"><a href="http://www.youtube.com/user/austmus/">Youtube</a><--->
	    <!--ul-->

            <!-- C356 reposition -->
           <!-- <div id="header-image"><?php echo theme_header_image(); ?></div>-->

           
           
        </header>
		
        <!-- END EMEL ENDING -->
        <!-- ?php echo theme_header_image(); ?--->
	
                      
    <div id="content" role="main" tabindex="-1">

<?php fire_plugin_hook('public_content_top', array('view'=>$this)); ?>
